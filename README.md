# Ruby Word Frequency

![](https://bafybeif2ifjuqusulgtqg774vz4577vdea32rxwzzyppsgpufeuwu7yz6a.ipfs.dweb.link/Screen%20Shot%202022-06-25%20at%2012.03.54%20PM.png)

![](https://bafybeiffnqq2hjcoyb56l33ut3xayowrwcojh6ejd22rxkoqxhhtnqiq3u.ipfs.dweb.link/Screen%20Shot%202022-06-25%20at%2012.04.09%20PM.png)

Ge the counts [here](https://bafybeid6c4vmggg5ag3cushen67ae2dlp2zvxff2unm66oi32xzyjma2vu.ipfs.dweb.link/counts.zip). The aggregated counts is in file word_frequency_in_ruby.csv. The cloned project list can be found in file counts/clone_top_ruby_projects.sh.

Watch this video https://www.youtube.com/watch?v=lKrL_Dyf44Y to understand more.
