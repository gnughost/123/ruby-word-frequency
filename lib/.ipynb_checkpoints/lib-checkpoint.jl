function counter(array_of_elements)
    counts = Dict()

    for element in array_of_elements
        counts[element] = get(counts, element, 0) + 1
    end

    counts
end

function word_count_in_a_string(string)
    words = strip(replace(string, r"\W+" => " "))
    word_array = split(words, r"\s+")
    counter(word_array)
end

function word_count_in_a_file(file_location)
    file_handler = open(file_location, "r")
    string = join(readlines(file_handler), "\n")
    close(file_handler)
    word_count_in_a_string(string)
end

function dict_to_counts_tuple(dictionary)
    array = []
    
    for (key, val) in dictionary
        push!(array, (val, key))
    end
    
    array
end

function count_file_to_dict(file_location)
    file_handler = open(file_location)
    lines = readlines(file_handler)
    close(file_handler)
    
    dict = Dict()

    for line in lines[2:length(lines)]
        count, word =  split(line, ',')
        dict[word] = parse(BigInt, count)
    end
    
    dict
end
